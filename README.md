<h1>Alphalogic Service Manager CLI
<a href="https://asm_cli.readthedocs.io">
<img src="https://readthedocs.org/projects/asm_cli/badge/?version=latest" alt="Documentation Status" />
</a>
</h1>
The official CLI for Alphalogic Service Manager. <a href="https://asm_cli.readthedocs.io">   Documentation</a>
