<p>&lt;h1&gt;Alphalogic Service Manager CLI
&lt;a href=”<a href="https://asm_cli.readthedocs.io" rel="nofollow">https://asm_cli.readthedocs.io</a>”&gt;
&lt;img src=”<a href="https://readthedocs.org/projects/asm_cli/badge/?version=latest" rel="nofollow">https://readthedocs.org/projects/asm_cli/badge/?version=latest</a>” alt=”Documentation Status” /&gt;
&lt;/a&gt;
&lt;/h1&gt;
The official CLI for Alphalogic Service Manager. &lt;a href=”<a href="https://asm_cli.readthedocs.io" rel="nofollow">https://asm_cli.readthedocs.io</a>”&gt;   Documentation&lt;/a&gt;</p>